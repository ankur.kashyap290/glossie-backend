var mongo = require('mongodb').MongoClient;
var express = require('express');
var app = express();
//Parse URL-encoded bodies
app.use(express.urlencoded()); 
//Used to parse JSON bodies
app.use(express.json()); 


//import our admin api
var adminApi = require('./api-v1-admin/admin');
//import our vendor api
var vendorApi = require('./api-v1-vendor/vendor');
//import our user api
var userApi = require('./api-v1-user/user');
//import our employee api
var employeeApi = require('./api-v1-employee/employee');

//variables
var mongodbBaseUrl = "mongodb://localhost:27017/";
var MONGO_DATABASE_NAME = "multiSalonDB";


//**(ADMIN)
app.post('/api-v1-admin/:name', function (req, res) {
   var name = req.params.name;
   const reqBody = req.body;

   //login
   if(name === 'login'){
         const colName = "adminCol";
         adminApi.executeApi(colName, res, name, reqBody ,
         mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
         return;
   }

   //check auth (MANDATORY)
   if(name === 'auth'){
     mAuth("auth");
     return;
   }

    //**HERE ALL ENDPOINTS WORKS WITH AUTH
    //add vendors
    if(name === 'add_vendor'){
      mAuth("add_vendor");
      return;
    }

    //remove vendor
    if(name === 'remove_vendor'){
      mAuth("remove_vendor");
      return;
   }

   //get all vendors
   if(name === 'get_vendors'){
      mAuth("get_vendors");
      return;
   }

   //get all users
   if(name === 'get_users'){
      mAuth("get_users");
      return;
   }

   //add coupons
   if(name === 'add_coupon'){
      mAuth("add_coupon");
      return;
   }

   //remove coupon
   if(name === 'remove_coupon'){
      mAuth("remove_coupon");
      return;
   }

   //get all coupons
   if(name === 'get_coupons'){
      mAuth("get_coupons");
      return;
   }

   //add category
   if(name === 'add_category'){
      mAuth("add_category");
      return;
   } 

   //get categories
   if(name === 'get_categories'){
      mAuth("get_categories");
      return;
   }

   //add banner
   if(name === 'add_banner'){
      mAuth("add_banner");
      return;
   }

   //remove banner
   if(name === 'remove_banner'){
      mAuth("remove_banner");
      return;
   }

   //get banners
   if(name === 'get_banners'){
      mAuth("get_banners");
      return;
   }

   //add offer
   if(name === 'add_offer'){
      mAuth("add_offer");
      return;
   }

   //remove offer
   if(name === 'remove_offer'){
      mAuth("remove_offer");
      return;
   }

   //get offers
   if(name === 'get_offers'){
      mAuth("get_offers");
      return;
   }

   //add home services
   if(name === 'add_home_services'){
      mAuth("add_home_services");
      return;
   }

   //remove home services
   if(name === 'remove_home_services'){
      mAuth("remove_home_services");
      return;
   }

   //get home services
   if(name === 'get_home_services'){
      mAuth("get_home_services");
      return;
   }

   //add employee
   if(name === 'add_employee'){
      mAuth("add_employee");
      return;
   }

   //remove employee
   if(name === 'remove_employee'){
      mAuth("remove_employee");
      return;
   }

   //get employees
   if(name === 'get_employees'){
      mAuth("get_employees");
      return;
   }


   //manage_bookings
   if(name === 'manage_bookings'){
      if(reqBody.hasOwnProperty("manageBookings") === true){
         if(reqBody.manageBookings.hasOwnProperty("booking_status") === true){
            if(reqBody.manageBookings.booking_status === 'approved'
            || reqBody.manageBookings.booking_status === 'canceled'){
               mAuth("manage_bookings");
               return;
            }else{
               res.json({"status":"invalid payload"});
            }
         }else{
            res.json({"status":"invalid payload"});
         }
      }else{
         res.json({"status":"invalid payload"});
      }
   }


    //auth checker
    function mAuth(getTask) {
      //validate auth data
      if(reqBody.hasOwnProperty("adminAuth") === true){
         if(reqBody.adminAuth.hasOwnProperty("email")
         && reqBody.adminAuth.hasOwnProperty("pass") ){
            //auth data is correct
             //connect to mongoDB
      const colName = "adminCol";
      const options = {useUnifiedTopology: true};
      mongo.connect(mongodbBaseUrl ,options, function(err, db) {
        if (err){
           //not connected to mongodb
           res.json({"status":"failed to authenticate, not connected to mongodb"});
        }else{
          //we are successfully connected to mongodb
          //specify our db name
          var myDB = db.db(MONGO_DATABASE_NAME);
           myDB.collection(colName).findOne({ "email":reqBody.adminAuth.email, "pass":reqBody.adminAuth.pass }, function(err, result) {
           if (err){
              res.json({"status":"failed to authenticate, not connected to mongodb"});
           }else{
               //check user login
               if(result !== null){
               //validate login
               if(result.email === reqBody.adminAuth.email 
                   && result.pass === reqBody.adminAuth.pass){
                       //**auth success
                       //testing auth
                       if(getTask === 'auth'){
                          res.json({"status":"auth success"});
                       }
                       //add_vendor
                       if(getTask === 'add_vendor'){
                        const colName = "vendorsCol";
                        adminApi.executeApi(colName, res, name, reqBody ,
                        mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
                       }
                       //remove_vendor
                       if(getTask === 'remove_vendor'){
                        const colName = "vendorsCol";
                        adminApi.executeApi(colName, res, name, reqBody ,
                        mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
                       }

                       //get_vendors
                       if(getTask === 'get_vendors'){
                        const colName = "vendorsCol";
                        adminApi.executeApi(colName, res, name, reqBody ,
                        mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
                       }

                       //get_users
                       if(getTask === 'get_users'){
                        const colName = "userCol";
                        adminApi.executeApi(colName, res, name, reqBody ,
                        mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
                       }

                       //add_coupon
                       if(getTask === 'add_coupon'){
                        const colName = "adminCouponCol";
                        adminApi.executeApi(colName, res, name, reqBody ,
                        mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
                       }

                       //remove_coupon
                       if(getTask === 'remove_coupon'){
                        const colName = "adminCouponCol";
                        adminApi.executeApi(colName, res, name, reqBody ,
                        mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
                       }

                       //get_coupons
                       if(getTask === 'get_coupons'){
                        const colName = "adminCouponCol";
                        adminApi.executeApi(colName, res, name, reqBody ,
                        mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
                       }

                       //add_category
                       if(getTask === 'add_category'){
                        const colName = "adminCategoryCol";
                        adminApi.executeApi(colName, res, name, reqBody ,
                        mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
                       }
                       
                       //get_categories
                       if(getTask === 'get_categories'){
                        const colName = "adminCategoryCol";
                        adminApi.executeApi(colName, res, name, reqBody ,
                        mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
                       }

                       
                       //add_banner
                       if(getTask === 'add_banner'){
                        const colName = "adminBannerCol";
                        adminApi.executeApi(colName, res, name, reqBody ,
                        mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
                       }

                       //remove_banner
                       if(getTask === 'remove_banner'){
                        const colName = "adminBannerCol";
                        adminApi.executeApi(colName, res, name, reqBody ,
                        mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
                       }

                       //get_banners
                       if(getTask === 'get_banners'){
                        const colName = "adminBannerCol";
                        adminApi.executeApi(colName, res, name, reqBody ,
                        mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
                       }

                     
                        //add_offer
                       if(getTask === 'add_offer'){
                        const colName = "adminOfferCol";
                        adminApi.executeApi(colName, res, name, reqBody ,
                        mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
                       }

                       //remove_offer
                       if(getTask === 'remove_offer'){
                        const colName = "adminOfferCol";
                        adminApi.executeApi(colName, res, name, reqBody ,
                        mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
                       }

                       //get_offers
                       if(getTask === 'get_offers'){
                        const colName = "adminOfferCol";
                        adminApi.executeApi(colName, res, name, reqBody ,
                        mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
                       }

                       //add_home_services
                       if(getTask === 'add_home_services'){
                        const colName = "adminHomeServicesCol";
                        adminApi.executeApi(colName, res, name, reqBody ,
                        mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
                       }

                       //remove_home_services
                       if(getTask === 'remove_home_services'){
                        const colName = "adminHomeServicesCol";
                        adminApi.executeApi(colName, res, name, reqBody ,
                        mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
                       }

                       //get_home_services
                       if(getTask === 'get_home_services'){
                        const colName = "adminHomeServicesCol";
                        adminApi.executeApi(colName, res, name, reqBody ,
                        mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
                       }


                       //add_employee
                       if(getTask === 'add_employee'){
                        const colName = "employeesCol";
                        adminApi.executeApi(colName, res, name, reqBody ,
                        mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
                       }

                       //remove_employee
                       if(getTask === 'remove_employee'){
                        const colName = "employeesCol";
                        adminApi.executeApi(colName, res, name, reqBody ,
                        mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
                       }

                       //get_employees
                       if(getTask === 'get_employees'){
                        const colName = "employeesCol";
                        adminApi.executeApi(colName, res, name, reqBody ,
                        mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
                       }

                       //manage_bookings
                       if(getTask === 'manage_bookings'){
                        const colName = "adminBookingCol";
                        adminApi.executeApi(colName, res, name, reqBody ,
                        mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
                       }


   
                   }else{
                       //invalid credentials
                       res.json({"status":"failed to authenticate, invalid credentials"});
                   }    
               }else{
                //invalid credentials
                res.json({"status":"failed to authenticate, invalid credentials"}); 
               }

               db.close();
           }
           
         });

        }

       
     });
         }else{
            res.json({"status":"invalid payload"});
         }
       }else{
          res.json({"status":"invalid payload"});
       }

     
     
     }

   //invalid endpoint
   res.send("invalid endpoint");
   
});


//**(VENDOR)
app.post('/api-v1-vendor/:name', function (req, res) {
   var name = req.params.name;
   const reqBody = req.body;

   //check expected endpoints
   if(name === 'login'){
        const colName = "vendorsCol";
        var qid = "n/a";
        vendorApi.executeApi(qid, colName, res, name, reqBody ,
        mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
        return;
   }

   //check auth (MANDATORY)
   if(name === 'auth'){
     mAuth("auth");
     return;
   }


    //**HERE ALL ENDPOINTS WORKS WITH AUTH
   //add services
   if(name === 'add_services'){
      mAuth("add_services");
      return;
   }

   //remove services
   if(name === 'remove_services'){
      mAuth("remove_services");
      return;
   }

   //get services
   if(name === 'get_services'){
      mAuth("get_services");
      return;
   }

   //add time slot
   if(name === 'add_time_slot'){
      mAuth("add_time_slot");
      return;
   }

   //remove time slot
   if(name === 'remove_time_slot'){
      mAuth("remove_time_slot");
      return;
   }

   //get time slots
   if(name === 'get_time_slots'){
      mAuth("get_time_slots");
      return;
   }

   //get bookings
   if(name === 'get_bookings'){
      mAuth("get_bookings");
      return;
   }

   
   //manage bookings
   if(name === 'manage_bookings'){
      if(reqBody.hasOwnProperty("manageBookings") === true){
         if(reqBody.manageBookings.hasOwnProperty("booking_status") === true){
            if(reqBody.manageBookings.booking_status === 'approved'
            || reqBody.manageBookings.booking_status === 'canceled'){
               mAuth("manage_bookings");
               return;
            }else{
               res.json({"status":"invalid payload"});
            }
         }else{
            res.json({"status":"invalid payload"});
         }
      }else{
         res.json({"status":"invalid payload"});
      }
   }


   //cancel bookings
   if(name === 'cancel_bookings'){
      if(reqBody.hasOwnProperty("cancelBookings") === true){
         if(reqBody.cancelBookings.hasOwnProperty("booking_status") === true){
            if(reqBody.cancelBookings.booking_status === 'canceled'){
               mAuth("cancel_bookings");
               return;
            }else{
               res.json({"status":"invalid payload"});
            }
         }else{
            res.json({"status":"invalid payload"});
         }
      }else{
         res.json({"status":"invalid payload"});
      }
   }


   //get reviews
   if(name === 'get_reviews'){
      mAuth("get_reviews");
      return;
   }



   //auth checker
    function mAuth(getTask) {
      //validate auth data
      if(reqBody.hasOwnProperty("vendorAuth") === true){
        if(reqBody.vendorAuth.hasOwnProperty("email")
        && reqBody.vendorAuth.hasOwnProperty("pass") ){
           //auth data is correct
//connect to mongoDB
const colName = "vendorsCol";
const options = {useUnifiedTopology: true};
mongo.connect(mongodbBaseUrl ,options, function(err, db) {
  if (err){
     //not connected to mongodb
     res.json({"status":"failed to authenticate, not connected to mongodb"});
  }else{
    //we are successfully connected to mongodb
    //specify our db name
    var myDB = db.db(MONGO_DATABASE_NAME);
     myDB.collection(colName).findOne({ "email":reqBody.vendorAuth.email, "pass":reqBody.vendorAuth.pass } , function(err, result) {
     if (err){
        res.json({"status":"failed to authenticate, not connected to mongodb"});
     }else{
         //check user login
         if(result !== null){
         //validate login
         if(result.email === reqBody.vendorAuth.email 
             && result.pass === reqBody.vendorAuth.pass){
                 //**auth success
                 //set qid
                 var qid = JSON.stringify(result._id);
                 qid = qid.replace('"','');
                 qid = qid.replace('"','');

                 //testing auth
                 if(getTask === 'auth'){
                    res.json({"status":"auth success"});
                 }

                 //add_services
                 if(getTask === 'add_services'){
                  const colName = "vendorServicesCol";
                  vendorApi.executeApi(qid, colName, res, name, reqBody ,
                  mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
                 }

                 //remove_services
                 if(getTask === 'remove_services'){
                  const colName = "vendorServicesCol";
                  vendorApi.executeApi(qid, colName, res, name, reqBody ,
                  mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
                 }

                 //get_services
                 if(getTask === 'get_services'){
                  const colName = "vendorServicesCol";
                  vendorApi.executeApi(qid, colName, res, name, reqBody ,
                  mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
                 }

                 //add_time_slot
                 if(getTask === 'add_time_slot'){
                  const colName = "vendorTimeSlotCol";
                  vendorApi.executeApi(qid, colName, res, name, reqBody ,
                  mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
                 }

                 //remove_time_slot
                 if(getTask === 'remove_time_slot'){
                  const colName = "vendorTimeSlotCol";
                  vendorApi.executeApi(qid, colName, res, name, reqBody ,
                  mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
                 }

                 //get_time_slots
                 if(getTask === 'get_time_slots'){
                  const colName = "vendorTimeSlotCol";
                  vendorApi.executeApi(qid, colName, res, name, reqBody ,
                  mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
                 }

                 //get_bookings
                 if(getTask === 'get_bookings'){
                  const colName = "vendorBookingCol";
                  vendorApi.executeApi(qid, colName, res, name, reqBody ,
                  mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
                 }

                 //manage_bookings
                 if(getTask === 'manage_bookings'){
                  const colName = "vendorBookingCol";
                  vendorApi.executeApi(qid, colName, res, name, reqBody ,
                  mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
                 }

                 //cancel_bookings
                 if(getTask === 'cancel_bookings'){
                  const colName = "vendorBookingCol";
                  vendorApi.executeApi(qid, colName, res, name, reqBody ,
                  mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
                 }

                 //get_reviews
                 if(getTask === 'get_reviews'){
                  const colName = "vendorReviewCol";
                  vendorApi.executeApi(qid, colName, res, name, reqBody ,
                  mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
                 }


             }else{
                 //invalid credentials
                 res.json({"status":"failed to authenticate, invalid credentials"});
             }    
         }else{
          //invalid credentials
          res.json({"status":"failed to authenticate, invalid credentials"}); 
         }

         db.close();
     }
     
   });

  }

 
});
        }else{
           res.json({"status":"invalid payload"});
        }
      }else{
         res.json({"status":"invalid payload"});
      }
    
    
   }

   //invalid endpoint
   res.send("invalid endpoint");
   
});


//**(USER)
app.post('/api-v1-user/:name', function (req, res) {
    var name = req.params.name;
    const reqBody = req.body;

    //check expected endpoints
    if(name === 'register' || name === 'login'){
       const colName = "userCol";
       var user_id = "n/a";
        userApi.executeApi(user_id, colName, res, name, reqBody ,
         mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
         return;
    }

    //check auth (MANDATORY)
    if(name === 'auth'){
      mAuth("auth");
      return;
    }


    //**HERE ALL ENDPOINTS WORKS WITH AUTH
    //get all vendors
   if(name === 'get_vendors'){
      mAuth("get_vendors");
      return;
   }

   //get vendor time slots
   if(name === 'get_vendor_time_slots'){
      mAuth("get_vendor_time_slots");
      return;
   }


   //get vendor services
   if(name === 'get_vendor_services'){
      mAuth("get_vendor_services");
      return;
   }

   
   //add vendor booking offline
   if(name === 'add_vendor_booking_offline'){
      //check payload must contains offline
      if(reqBody.hasOwnProperty("addVendorBookingOffline") === true){
         if(reqBody.addVendorBookingOffline.hasOwnProperty("payment_details") === true){
            if(reqBody.addVendorBookingOffline.payment_details.hasOwnProperty("payment_type") === true){
              if(reqBody.addVendorBookingOffline.payment_details.payment_type === 'offline'){
               mAuth("add_vendor_booking_offline");
              }else{
               res.json({"status":"invalid payload"});
            }
            }else{
               res.json({"status":"invalid payload"});
            }
         }else{
            res.json({"status":"invalid payload"});
         }
      }else{
         res.json({"status":"invalid payload"});
      }
      return;
   }

   //get user notifications
   if(name === 'get_user_notifications'){
      mAuth("get_user_notifications");
      return;
   }

   //get admin services
   if(name === 'get_admin_services'){
      mAuth("get_admin_services");
      return;
   }


   //add admin booking offline
   if(name === 'add_admin_booking_offline'){
      mAuth("add_admin_booking_offline");
      return;
   }


   //add employee review
   if(name === 'add_employee_review'){
      mAuth("add_employee_review");
      return;
   }


   //add_vendor_review
   if(name === 'add_vendor_review'){
      mAuth("add_vendor_review");
      return;
   }



    //auth checker
    function mAuth(getTask) {
       //validate auth data
       if(reqBody.hasOwnProperty("userAuth") === true){
         if(reqBody.userAuth.hasOwnProperty("email")
         && reqBody.userAuth.hasOwnProperty("pass") ){
            //auth data is correct
 //connect to mongoDB
 const colName = "userCol";
 const options = {useUnifiedTopology: true};
 mongo.connect(mongodbBaseUrl ,options, function(err, db) {
   if (err){
      //not connected to mongodb
      res.json({"status":"failed to authenticate, not connected to mongodb"});
   }else{
     //we are successfully connected to mongodb
     //specify our db name
     var myDB = db.db(MONGO_DATABASE_NAME);
      myDB.collection(colName).findOne({ "email":reqBody.userAuth.email, "pass":reqBody.userAuth.pass }, function(err, result) {
      if (err){
         res.json({"status":"failed to authenticate, not connected to mongodb"});
      }else{
          //check user login
          if(result !== null){
          //validate login
          if(result.email === reqBody.userAuth.email 
              && result.pass === reqBody.userAuth.pass){
                  //**auth success
                  //set user_id
                 var user_id = JSON.stringify(result._id);
                 user_id = user_id.replace('"','');
                 user_id = user_id.replace('"','');
                  //testing auth
                  if(getTask === 'auth'){
                     res.json({"status":"auth success"});
                  }

                  //get_vendors
                  if(getTask === 'get_vendors'){
                     const colName = "vendorsCol";
                     userApi.executeApi(user_id, colName, res, name, reqBody ,
                     mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
                    }

                  //get_vendor_time_slots  
                  if(getTask === 'get_vendor_time_slots'){
                     const colName = "vendorTimeSlotCol";
                     userApi.executeApi(user_id, colName, res, name, reqBody ,
                     mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
                    }

                  //get_vendor_services  
                  if(getTask === 'get_vendor_services'){
                     const colName = "vendorServicesCol";
                     userApi.executeApi(user_id, colName, res, name, reqBody ,
                     mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
                    }

                  //add_vendor_booking_offline  
                  if(getTask === 'add_vendor_booking_offline'){
                     const colName = "vendorBookingCol";
                     userApi.executeApi(user_id, colName, res, name, reqBody ,
                     mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
                    }

                    //get_user_notifications
                    if(getTask === 'get_user_notifications'){
                     const colName = "userNotificationsCol";
                     userApi.executeApi(user_id, colName, res, name, reqBody ,
                     mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
                    }

                    //get_admin_services
                    if(getTask === 'get_admin_services'){
                     const colName = "adminHomeServicesCol";
                     userApi.executeApi(user_id, colName, res, name, reqBody ,
                     mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
                    }

                    //add_admin_booking_offline
                    if(getTask === 'add_admin_booking_offline'){
                     const colName = "adminBookingCol";
                     userApi.executeApi(user_id, colName, res, name, reqBody ,
                     mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
                    }

                    //add_employee_review
                    if(getTask === 'add_employee_review'){
                     const colName = "employeeReviewCol";
                     userApi.executeApi(user_id, colName, res, name, reqBody ,
                     mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
                    }

                    //add_vendor_review
                    if(getTask === 'add_vendor_review'){
                     const colName = "vendorReviewCol";
                     userApi.executeApi(user_id, colName, res, name, reqBody ,
                     mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
                    }

              }else{
                  //invalid credentials
                  res.json({"status":"failed to authenticate, invalid credentials"});
              }    
          }else{
           //invalid credentials
           res.json({"status":"failed to authenticate, invalid credentials"}); 
          }

          db.close();
      }
      
    });

   }

  
});
         }else{
            res.json({"status":"invalid payload"});
         }
       }else{
          res.json({"status":"invalid payload"});
       }
     
     
     }

    //invalid endpoint
    res.send("invalid endpoint");
    
 });



//**(EMPLOYEE)
app.post('/api-v1-employee/:name' , function (req , res) {
   var name = req.params.name;
   const reqBody = req.body;

   //check expected endpoints
   if(name === 'login'){
        const colName = "employeesCol";
        var employee_id = "n/a";
        employeeApi.executeApi(employee_id, colName, res, name, reqBody ,
        mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
        return;
   }

   //check auth (MANDATORY)
   if(name === 'auth'){
     mAuth("auth");
     return;
   }

    //get user notifications
   if(name === 'get_employee_notifications'){
      mAuth("get_employee_notifications");
      return;
   }

    //get reviews
    if(name === 'get_reviews'){
      mAuth("get_reviews");
      return;
   }

//auth checker
function mAuth(getTask) {
   //validate auth data
   if(reqBody.hasOwnProperty("employeeAuth") === true){
     if(reqBody.employeeAuth.hasOwnProperty("email")
     && reqBody.employeeAuth.hasOwnProperty("pass") ){
        //auth data is correct
//connect to mongoDB
const colName = "employeesCol";
const options = {useUnifiedTopology: true};
mongo.connect(mongodbBaseUrl ,options, function(err, db) {
if (err){
  //not connected to mongodb
  res.json({"status":"failed to authenticate, not connected to mongodb"});
}else{
 //we are successfully connected to mongodb
 //specify our db name
 var myDB = db.db(MONGO_DATABASE_NAME);
  myDB.collection(colName).findOne({ "email":reqBody.employeeAuth.email, "pass":reqBody.employeeAuth.pass } , function(err, result) {
  if (err){
     res.json({"status":"failed to authenticate, not connected to mongodb"});
  }else{
      //check user login
      if(result !== null){
      //validate login
      if(result.email === reqBody.employeeAuth.email 
          && result.pass === reqBody.employeeAuth.pass){
              //**auth success
              //set employee_id
              var employee_id = JSON.stringify(result._id);
              employee_id = employee_id.replace('"','');
              employee_id = employee_id.replace('"','');

              //testing auth
              if(getTask === 'auth'){
                 res.json({"status":"auth success"});
              }

              //get_employee_notifications
              if(getTask === 'get_employee_notifications'){
               const colName = "employeeNotificationsCol";
               employeeApi.executeApi(employee_id, colName, res, name, reqBody ,
               mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
              }

              //get_reviews
              if(getTask === 'get_reviews'){
               const colName = "employeeReviewCol";
               employeeApi.executeApi(employee_id, colName, res, name, reqBody ,
               mongo, mongodbBaseUrl, MONGO_DATABASE_NAME);
              }

          }else{
              //invalid credentials
              res.json({"status":"failed to authenticate, invalid credentials"});
          }    
      }else{
       //invalid credentials
       res.json({"status":"failed to authenticate, invalid credentials"}); 
      }

      db.close();
  }
  
});

}


});
     }else{
        res.json({"status":"invalid payload"});
     }
   }else{
      res.json({"status":"invalid payload"});
   }
 
 
}

//invalid endpoint
res.send("invalid endpoint");



});
 

 
 var server = app.listen(8081, function () {
    var host = server.address().address
    var port = server.address().port
    
    console.log("App listening at http://%s:%s", host, port)
 })