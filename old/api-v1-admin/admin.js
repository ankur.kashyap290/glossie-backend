const { ObjectId, ObjectID } = require("bson");

(function () {
    
    module.exports.executeApi = function(colName, res, name, reqBody, mongo, mongodbBaseUrl, MONGO_DATABASE_NAME) {

        //connect to mongoDB
       const options = {useUnifiedTopology: true};
       mongo.connect(mongodbBaseUrl ,options, function(err, db) {
        if (err){
           //not connected to mongodb
           throw err;
        }else{
          //we are successfully connected to mongodb
          //console.log("Connected to mongodb");
          //specify our db name
          var myDB = db.db(MONGO_DATABASE_NAME);

          //**LOGIN
          if(name === 'login'){
            myDB.collection(colName).findOne({ "email":reqBody.email, "pass":reqBody.pass }, function(err, result) {
                if (err){
                    res.json({"status":"err"});
                    return;
                }else{
                    //check user login
                    if(result !== null){
                    //validate login
                    if(result.email === reqBody.email 
                        && result.pass === reqBody.pass){
                            const userLoggedInData = { "status":"login success" , "email":result.email, "name":result.name, "number":result.number };
                            //**login success
                            res.json(userLoggedInData);
                        }else{
                            //invalid credentials
                            res.json( {"status":"login failed invalid credentials"} ); 
                        }    
                    }else{
                     //invalid credentials
                     res.json( {"status":"login failed invalid credentials"} ); 
                    }

                    db.close();
                    return;
                }
                
              });
          }

          //**ADD VENDOR
          if(name === 'add_vendor'){
            //check if user already exists or not
            //check number
            myDB.collection(colName).findOne({"number":reqBody.addVendor.number}, function(err, result) {
              if (err){
                  //an err occured
                  res.json({"status":"err"});
                  return;
              }else{
                  if(result !== null){
                      //number already exists
                      res.json({"status":"number_already_exists"});
                      return;
                  }else{
                     //number not exists, now we check email
                     myDB.collection(colName).findOne({"email":reqBody.addVendor.email}, function(err, result) {
                      if (err){
                          //an err occured
                          res.json({"status":"err"});
                          return;
                      }else{
                          if(result !== null){
                              //email already exists
                              res.json({"status":"email_already_exists"});
                              return;
                          }else{
                             //number and email not exists now we check salon_name
                       myDB.collection(colName).findOne({"salon_name":reqBody.addVendor.salon_name}, function(err, result) {
                        if (err){
                            //an err occured
                            res.json({"status":"err"});
                            return;
                        }else{
                            if(result !== null){
                                //name already exists
                                res.json({"status":"salon_name_already_exists"});
                                return;
                            }else{
                               //number, email and name not exists
                               //**user not exists, now procced to create new user account
                               var currentdate = new Date(); 
                               
                                               var datetime =  currentdate.getFullYear() + "-"
                                               + (currentdate.getMonth()+1)  + "-" 
                                               + currentdate.getDate() + " "  
                                               + currentdate.getHours() + ":"  
                                               + currentdate.getMinutes();              


                              reqBody.addVendor.created_at = datetime;
                              reqBody.addVendor.updated_at = datetime;
                              myDB.collection(colName).insertOne(reqBody.addVendor, function(err, result) {
                                if (err){
                                   res.json({"status":"err"});
                                }else{
                                  if(result !== null){
                                    res.json({"status":"Vendor registered successfully"}); 
                                  }else{
                                    res.json({"status":"err"});
                                  }
                               }

                               //close db connection
                               db.close();
                               return;
                           
                         });
                                
            
                            }
                        }
                      });
                              
          
                          }
                      }
                    });
                      
  
                  }
              }
            });
  
            }

          //**REMOVE VENDOR
          if(name === 'remove_vendor'){
            //remove vendor by salon_name  
            myDB.collection(colName).deleteOne({"salon_name":reqBody.removeVendor.salon_name}, function(err, result) {
              if (err){
                res.json({"status":"err"});
                return;
              }else{
                //check if removed success
                if(result.deletedCount === 1){
                    res.json({"status":"Vendor removed successfully"});
                }else{
                    res.json({"status":"err or incorrect salon name"});
                }
                db.close();
                return;
              }
            });
          }  

          //**GET ALL VENDORS
          if(name === 'get_vendors'){

            const responseParams =  { _id: 0,
                "number": 1,
                "email": 2,
                "name": 3,
                "description": 4,
                "salon_name": 5,
                "salon_for": 6,
                "website_name": 7,
                "salon_img": 8,
                "salon_logo": 9,
                "salon_address": 10,
                "salon_zipcode": 11,
                "salon_city": 12,
                "salon_state": 13,
                "salon_country": 14,
                "salon_timing": 15,
                "created_at": 16,
                "updated_at": 17
        };

            myDB.collection(colName).find({}, { projection:  responseParams } ).limit(reqBody.getVendors.limit).toArray(function(err, result) {
                if (err){
                    res.json({"status":"err"});
                }else{
                    res.json({ "status":"vendors loaded success", result });
                    db.close();
                    return;
                }
              });
          }

          //**GET ALL USERS
          if(name === 'get_users'){
            const responseParams =  { _id: 0, 'number': 1, 'email': 2, 'name': 3 };  
            myDB.collection(colName).find({}, { projection:  responseParams } ).limit(reqBody.getUsers.limit).toArray(function(err, result) {
                if (err){
                    res.json({"status":"err"});
                }else{
                    res.json({ "status":"users loaded success", result });
                    db.close();
                    return;
                }
              });
          }

          //**ADD COUPON
          if(name === 'add_coupon'){
            //check coupon code
            myDB.collection(colName).findOne({"code":reqBody.addCoupon.code}, function(err, result) {
                if (err){
                    //an err occured
                    res.json({"status":"err"});
                    return;
                }else{
                    if(result !== null){
                        //coupon code already exists
                        res.json({"status":"coupon_code_already_exists"});
                        return;
                    }else{
                      //**coupon code not exists, now procced to add coupon
                      myDB.collection(colName).insertOne(reqBody.addCoupon, function(err, result) {
                        if (err){
                           res.json({"status":"err"});
                        }else{
                          if(result !== null){
                            res.json({"status":"Coupon created successfully"}); 
                          }else{
                            res.json({"status":"err"});
                          }
                       }

                       //close db connection
                       db.close();
                       return;
                   
                 });
                        
    
                    }
                }
              });


          }

          //**REMOVE COUPON
          if(name === 'remove_coupon'){
            //remove coupon by code  
            myDB.collection(colName).deleteOne({"code":reqBody.removeCoupon.code}, function(err, result) {
                if (err){
                  res.json({"status":"err"});
                  return;
                }else{
                  //check if removed success
                  if(result.deletedCount === 1){
                      res.json({"status":"Coupon removed successfully"});
                  }else{
                      res.json({"status":"err or incorrect coupon code"});
                  }
                  db.close();
                  return;
                }
              });
           }

           //**GET ALL COUPONS
          if(name === 'get_coupons'){
            const responseParams =  { _id: 0,
                "code": 1,
                "description": 2,
                "discount": 3,
                "maximum_use": 4,
                "start_date": 5,
                "end_date": 6
                 };  

            myDB.collection(colName).find({}, { projection:  responseParams }).limit(reqBody.getCoupons.limit).toArray(function(err, result) {
                if (err){
                    res.json({"status":"err"});
                }else{
                    res.json({ "status":"coupons loaded success", result });
                    db.close();
                    return;
                }
              });
          }

          //**ADD CATEGORY
          if(name === 'add_category'){
            //check category
            myDB.collection(colName).findOne({"category":reqBody.addCategory.category}, function(err, result) {
                if (err){
                    //an err occured
                    res.json({"status":"err"});
                    return;
                }else{
                    if(result !== null){
                        //category already exists
                        res.json({"status":"category_already_exists"});
                        return;
                    }else{
                      //**category not exists, now procced to add category
                      var currentdate = new Date(); 
                               var datetime =  currentdate.getFullYear() + "-"
                                               + (currentdate.getMonth()+1)  + "-" 
                                               + currentdate.getDate() + " "  
                                               + currentdate.getHours() + ":"  
                                               + currentdate.getMinutes();               
                              reqBody.addCategory.created_at = datetime;
                              reqBody.addCategory.updated_at = datetime;
                      myDB.collection(colName).insertOne(reqBody.addCategory, function(err, result) {
                        if (err){
                           res.json({"status":"err"});
                        }else{
                          if(result !== null){
                            res.json({"status":"Category created successfully"}); 
                          }else{
                            res.json({"status":"err"});
                          }
                       }

                       //close db connection
                       db.close();
                       return;
                   
                 });
                        
    
                    }
                }
              });


          }

          //**GET CATEGORIES
          if(name === 'get_categories'){
            const responseParams =  { _id: 0,
                "status": 1,
                "image": 2,
                "category": 3,
                "created_at": 4,
                "updated_at": 5
                 };  
                 
            myDB.collection(colName).find({}, { projection:  responseParams }).limit(reqBody.getCategories.limit).toArray(function(err, result) {
                if (err){
                    res.json({"status":"err"});
                }else{
                    res.json({ "status":"categories loaded success", result });
                    db.close();
                    return;
                }
              });

          }

          //**ADD BANNER
          if(name === 'add_banner'){
              //check banner
            myDB.collection(colName).findOne({"title":reqBody.addBanner.title}, function(err, result) {
                if (err){
                    //an err occured
                    res.json({"status":"err"});
                    return;
                }else{
                    if(result !== null){
                        //banner already exists
                        res.json({"status":"banner_already_exists"});
                        return;
                    }else{
                      //**banner not exists, now procced to add banner
                      var currentdate = new Date(); 
                               var datetime =  currentdate.getFullYear() + "-"
                                               + (currentdate.getMonth()+1)  + "-" 
                                               + currentdate.getDate() + " "  
                                               + currentdate.getHours() + ":"  
                                               + currentdate.getMinutes();               
                              reqBody.addBanner.created_at = datetime;
                              reqBody.addBanner.updated_at = datetime;
                      myDB.collection(colName).insertOne(reqBody.addBanner, function(err, result) {
                        if (err){
                           res.json({"status":"err"});
                        }else{
                          if(result !== null){
                            res.json({"status":"Banner created successfully"}); 
                          }else{
                            res.json({"status":"err"});
                          }
                       }

                       //close db connection
                       db.close();
                       return;
                   
                 });
                        
    
                    }
                }
              });
          }

          //**REMOVE BANNER
          if(name === 'remove_banner'){
            //remove banner by title  
            myDB.collection(colName).deleteOne({"title":reqBody.removeBanner.title}, function(err, result) {
                if (err){
                  res.json({"status":"err"});
                  return;
                }else{
                  //check if removed success
                  if(result.deletedCount === 1){
                      res.json({"status":"Banner removed successfully"});
                  }else{
                      res.json({"status":"err or incorrect banner title"});
                  }
                  db.close();
                  return;
                }
              });
          }

          //**GET ALL BANNERS
          if(name === 'get_banners'){
            const responseParams =  { _id: 0,
                "status": 1,
                "image": 2,
                "title": 3,
                "created_at": 4,
                "updated_at": 5
                 };  
                 
            myDB.collection(colName).find({}, { projection:  responseParams }).limit(reqBody.getBanners.limit).toArray(function(err, result) {
                if (err){
                    res.json({"status":"err"});
                }else{
                    res.json({ "status":"banners loaded success", result });
                    db.close();
                    return;
                }
              });
          }

          //**ADD OFFER
          if(name === 'add_offer'){
            //check offer title
            myDB.collection(colName).findOne({"title":reqBody.addOffer.title}, function(err, result) {
                if (err){
                    //an err occured
                    res.json({"status":"err"});
                    return;
                }else{
                    if(result !== null){
                        //banner already exists
                        res.json({"status":"offer_already_exists"});
                        return;
                    }else{
                      //**offer title not exists, now procced to add offer
                      var currentdate = new Date(); 
                               var datetime =  currentdate.getFullYear() + "-"
                                               + (currentdate.getMonth()+1)  + "-" 
                                               + currentdate.getDate() + " "  
                                               + currentdate.getHours() + ":"  
                                               + currentdate.getMinutes();               
                              reqBody.addOffer.created_at = datetime;
                              reqBody.addOffer.updated_at = datetime;
                      myDB.collection(colName).insertOne(reqBody.addOffer, function(err, result) {
                        if (err){
                           res.json({"status":"err"});
                        }else{
                          if(result !== null){
                            res.json({"status":"Offer created successfully"}); 
                          }else{
                            res.json({"status":"err"});
                          }
                       }

                       //close db connection
                       db.close();
                       return;
                   
                 });
                        
    
                    }
                }
              });
          }

          //**REMOVE OFFER
          if(name === 'remove_offer'){
            //remove offer by title  
            myDB.collection(colName).deleteOne({"title":reqBody.removeOffer.title}, function(err, result) {
                if (err){
                  res.json({"status":"err"});
                  return;
                }else{
                  //check if removed success
                  if(result.deletedCount === 1){
                      res.json({"status":"Offer removed successfully"});
                  }else{
                      res.json({"status":"err or incorrect offer title"});
                  }
                  db.close();
                  return;
                }
              });
          }

          //**GET ALL OFFERS
          if(name === 'get_offers'){
            const responseParams =  { _id: 0,
                "status": 1,
                "image": 2,
                "title": 3,
                "discount": 4,
                "created_at": 5,
                "updated_at": 6
                 };  
                 
            myDB.collection(colName).find({}, { projection:  responseParams }).limit(reqBody.getOffers.limit).toArray(function(err, result) {
                if (err){
                    res.json({"status":"err"});
                }else{
                    res.json({ "status":"offers loaded success", result });
                    db.close();
                    return;
                }
              });
          }

          //**ADD HOME SERVICES
          if(name === 'add_home_services'){
            //check name
            myDB.collection(colName).findOne({"name":reqBody.addHomeServices.name}, function(err, result) {
              if (err){
                  //an err occured
                  res.json({"status":"err"});
                  return;
              }else{
                  if(result !== null){
                      //service name already exists
                      res.json({"status":"service_already_exists"});
                      return;
                  }else{
                    //**service not exists, now procced to add service
                    myDB.collection(colName).insertOne(reqBody.addHomeServices, function(err, result) {
                      if (err){
                         res.json({"status":"err"});
                      }else{
                        if(result !== null){
                          res.json({"status":"Service created successfully"}); 
                        }else{
                          res.json({"status":"err"});
                        }
                     }

                     //close db connection
                     db.close();
                     return;
                 
               });
                      
  
                  }
              }
            });
          }

          //**REMOVE HOME SERVICES
          if(name === 'remove_home_services'){
             //remove service by name  
             myDB.collection(colName).deleteOne({"name":reqBody.removeHomeServices.name}, function(err, result) {
              if (err){
                res.json({"status":"err"});
                return;
              }else{
                //check if removed success
                if(result.deletedCount === 1){
                    res.json({"status":"Service removed successfully"});
                }else{
                    res.json({"status":"err or incorrect service name"});
                }
                db.close();
                return;
              }
            });
          }

          //**GET ALL HOME SERVICES
          if(name === 'get_home_services'){
            const responseParams =  { _id: 0,
            "status": 1,
            "name": 2,
            "category": 3,
            "image": 4,
            "service_for": 5,
            "service_price": 6,
            "service_minutes": 7
               };
               
          myDB.collection(colName).find({}, { projection:  responseParams }).limit(reqBody.getHomeServices.limit).toArray(function(err, result) {
              if (err){
                  res.json({"status":"err"});
              }else{
                  res.json({ "status":"home services loaded success", result });
                  db.close();
                  return;
              }
            });
          }

          //**ADD EMPLOYEE
          if(name === 'add_employee'){
            //check if employee already exists or not
          //check number
          myDB.collection(colName).findOne({"number":reqBody.addEmployee.number}, function(err, result) {
            if (err){
                //an err occured
                res.json({"status":"err"});
                return;
            }else{
                if(result !== null){
                    //number already exists
                    res.json({"status":"number_already_exists"});
                    return;
                }else{
                   //number not exists, now we check email
                   myDB.collection(colName).findOne({"email":reqBody.addEmployee.email}, function(err, result) {
                    if (err){
                        //an err occured
                        res.json({"status":"err"});
                        return;
                    }else{
                        if(result !== null){
                            //email already exists
                            res.json({"status":"email_already_exists"});
                            return;
                        }else{
                           //number and email not exists
                            //**employee not exists, now procced to create new employee account
                           myDB.collection(colName).insertOne(reqBody.addEmployee, function(err, result) {
                             if (err){
                                res.json({"status":"err"});
                             }else{
                               if(result !== null){
                                res.json({"status":"Employee registered successfully"});
                               }else{
                                res.json({"status":"err"});
                               }
                            }
  
                            //close db connection
                            db.close();
                            return;
                        
                      });
        
                        }
                    }
                  });
                    

                }
            }
          });

          }

          //**REMOVE EMPLOYEE
          if(name === 'remove_employee'){
                 //remove employee by email  
                 myDB.collection(colName).deleteOne({"email":reqBody.removeEmployee.employee_email}, function(err, result) {
                  if (err){
                    res.json({"status":"err"});
                    return;
                  }else{
                    //check if removed success
                    if(result.deletedCount === 1){
                        res.json({"status":"Employee removed successfully"});
                    }else{
                        res.json({"status":"err or incorrect employee email"});
                    }
                    db.close();
                    return;
                  }
                });
          } 

          //**GET ALL EMPLOYEES
          if(name === 'get_employees'){
            const responseParams =  { _id: 0,
              "number": 1,
              "email": 2,
              "name": 3
            };

          myDB.collection(colName).find({}, { projection:  responseParams } ).limit(reqBody.getEmployees.limit).toArray(function(err, result) {
              if (err){
                  res.json({"status":"err"});
              }else{
                  res.json({ "status":"employees loaded success", result });
                  db.close();
                  return;
              }
            });

          }


          //**MANAGE BOOKINGS (approved and canceled)
          if(name === 'manage_bookings'){
            //booking status should be pending and booking id should be matched

            //check _id
            if(ObjectID.isValid(reqBody.manageBookings.booking_id)){

              var myquery = { 
                "_id":ObjectId(reqBody.manageBookings.booking_id),
                "booking_status":"pending"
           };
            var newvalues = { 
              "booking_status":reqBody.manageBookings.booking_status
          };
          
            myDB.collection(colName).updateOne( myquery , {$set: newvalues }, function(err, result) {
              if (err){
                res.json({"status":"err"});
              }else{
                if(result !== null){
                  //check if updated or not
                  if(result.modifiedCount === 1){
                    //status updated success
                  res.json({"status":"status_updated_success"});

                       //**booking status is updated, now we send notification to user
                       //create user notification payload
                       //date and time
                       var currentdate = new Date(); 
                       var date =  currentdate.getFullYear() + "-"
                                       + (currentdate.getMonth()+1)  + "-" 
                                       + currentdate.getDate();     

                                       var time = currentdate.getHours() + ":"  
                                       + currentdate.getMinutes(); 

                       
                       var userNotiPayload;             
                       if(reqBody.manageBookings.booking_status === 'approved'){
                        //msg on approved
                         userNotiPayload = {
                          "user_id": reqBody.manageBookings.notifyUser.user_id,
                          "message": "Hi, "+reqBody.manageBookings.notifyUser.user_name + " your booking is "
                          +reqBody.manageBookings.booking_status + " for home service with " + reqBody.manageBookings.notifyUser.employee_name,
                          "employee_id": reqBody.manageBookings.notifyUser.employee_id,
                          "date":date,
                          "time":time
                         };
                       }else{
                         //msg on canceled
                          userNotiPayload = {
                          "user_id": reqBody.manageBookings.notifyUser.user_id,
                          "message": "Hi, "+reqBody.manageBookings.notifyUser.user_name + " your booking is "
                          +reqBody.manageBookings.booking_status + " for home service ",
                          "date":date,
                          "time":time
                         };
                       }
                      
                      myDB.collection("userNotificationsCol").insertOne(userNotiPayload, function(err, result) {
                          if (err){
                             //res.json({"status":"err"});
                          }else{
                            if(result !== null){
                              //res.json({"status":"notification sended to user success"}); 
                            }else{
                              //res.json({"status":"err"});
                            }


                            //**now send notification to employee
                            if(reqBody.manageBookings.booking_status === 'approved'){

                             var employeeNotiPayload = {
                                "employee_id": reqBody.manageBookings.notifyUser.employee_id,
                                "message": "Hi, "+reqBody.manageBookings.notifyUser.employee_name + " you assigned for "
                                +reqBody.manageBookings.notifyUser.list_of_services + " on home service with " + reqBody.manageBookings.notifyUser.user_name,
                                "user_address":reqBody.manageBookings.notifyUser.user_address,
                                "date":date,
                                "time":time
                               };

                           myDB.collection("employeeNotificationsCol").insertOne(employeeNotiPayload, function(err, result) {
                          if (err){
                             //res.json({"status":"err"});
                          }else{
                            if(result !== null){
                              //res.json({"status":"notification sended to user success"}); 
                            }else{
                              //res.json({"status":"err"});
                            }

                            //close db connection
                         db.close();
                         return;

                         
                         }
                          });
                            }else{
                              //close db connection
                             db.close();
                             return;
                            }
                         

                         }
                      });

                  }else{
                    res.json({"status":"err"});
                  }

              }else{
                res.json({"status":"err"});
              }


              }
                    
            });
               
            }else{
              res.json({"status":"invalid booking_id"});
              return;
            }
          }

        }
        
    });
        
    }

}());


                            
