const { ObjectId, ObjectID } = require("bson");

(function() {
    module.exports.executeApi = function(qid, colName, res, name, reqBody, mongo, mongodbBaseUrl, MONGO_DATABASE_NAME) {

       //connect to mongoDB
       const options = {useUnifiedTopology: true};
       mongo.connect(mongodbBaseUrl ,options, function(err, db) {
        if (err){
           //not connected to mongodb
           throw err;
        }else{
          //we are successfully connected to mongodb
          //console.log("Connected to mongodb");
          //specify our db name
          var myDB = db.db(MONGO_DATABASE_NAME);

          //**LOGIN
          if(name === 'login'){
            myDB.collection(colName).findOne({ "email":reqBody.email, "pass":reqBody.pass }, function(err, result) {
                if (err){
                    res.json({"status":"err"});
                    return;
                }else{
                    //check user login
                    if(result !== null){
                    //validate login
                    if(result.email === reqBody.email 
                        && result.pass === reqBody.pass){
                            //remove some keys
                            delete result._id;
                            delete result.pass;
                            delete result.created_at;
                            delete result.updated_at;
                            //**login success
                            res.json({"status":"login success", result});
                        }else{
                            //invalid credentials
                            res.json( {"status":"login failed invalid credentials"} ); 
                        }    
                    }else{
                     //invalid credentials
                     res.json( {"status":"login failed invalid credentials"} ); 
                    }

                    db.close();
                    return;
                }
                
              });
          }

            //**ADD SERVICES
          if(name === 'add_services'){
            //check name
            myDB.collection(colName).findOne({"name":reqBody.addServices.name, "qid":qid }, function(err, result) {
              if (err){
                  //an err occured
                  res.json({"status":"err"});
                  return;
              }else{
                  if(result !== null){
                      //service name already exists
                      res.json({"status":"service_already_exists"});
                      return;
                  }else{
                    
                    //**service not exists, now procced to add service
                    //add qid in service
                    reqBody.addServices.qid = qid;
                    myDB.collection(colName).insertOne(reqBody.addServices, function(err, result) {
                        if (err){
                           res.json({"status":"err"});
                        }else{
                          if(result !== null){
                            res.json({"status":"Service created successfully"}); 
                          }else{
                            res.json({"status":"err"});
                          }
                       }
  
                       //close db connection
                       db.close();
                       return;
                 
               });
  
                  }
              }
            });
          }

          //**REMOVE SERVICES
          if(name === 'remove_services'){
             //remove service by name  
             myDB.collection(colName).deleteOne({"name":reqBody.removeServices.name, "qid":qid }, function(err, result) {
              if (err){
                res.json({"status":"err"});
                return;
              }else{
                //check if removed success
                if(result.deletedCount === 1){
                    res.json({"status":"Service removed successfully"});
                }else{
                    res.json({"status":"err or incorrect service name"});
                }
                db.close();
                return;
              }
            });
          }

          //**GET ALL SERVICES
          if(name === 'get_services'){
            const responseParams =  { _id: 0,
            "status": 1,
            "name": 2,
            "category": 3,
            "image": 4,
            "service_for": 5,
            "service_price": 6,
            "service_minutes": 7
               };
               
          myDB.collection(colName).find({ "qid":qid }, { projection:  responseParams }).limit(reqBody.getServices.limit).toArray(function(err, result) {
              if (err){
                  res.json({"status":"err"});
              }else{
                  res.json({ "status":"services loaded success", result });
                  db.close();
                  return;
              }
            });
          }

          //**ADD TIME SLOT
          if(name === 'add_time_slot'){
            //check date & time
            myDB.collection(colName).findOne({"date":reqBody.addTimeSlot.date, "time":reqBody.addTimeSlot.time, "qid":qid }, function(err, result) {
                if (err){
                    //an err occured
                    res.json({"status":"err"});
                    return;
                }else{
                    if(result !== null){
                        //time already exists
                        res.json({"status":"time_already_exists"});
                        return;
                    }else{
                      
                      //**time not exists, now procced to add time
                      //add qid in service
                      reqBody.addTimeSlot.qid = qid;
                      myDB.collection(colName).insertOne(reqBody.addTimeSlot, function(err, result) {
                          if (err){
                             res.json({"status":"err"});
                          }else{
                            if(result !== null){
                              res.json({"status":"Time created successfully"}); 
                            }else{
                              res.json({"status":"err"});
                            }
                         }
    
                         //close db connection
                         db.close();
                         return;
                   
                 });
    
                    }
                }
              });
          }


          //**REMOVE TIME SLOT
          if(name === 'remove_time_slot'){
             //remove time
             myDB.collection(colName).deleteOne({"date":reqBody.removeTimeSlot.date, "time":reqBody.removeTimeSlot.time, "qid":qid }, function(err, result) {
              if (err){
                res.json({"status":"err"});
                return;
              }else{
                //check if removed success
                if(result.deletedCount === 1){
                    res.json({"status":"Time removed successfully"});
                }else{
                    res.json({"status":"err or incorrect time"});
                }
                db.close();
                return;
              }
            });

          }


          //**GET ALL TIME SLOTS
          if(name === 'get_time_slots'){
            const responseParams =  { _id: 0,
                "date":1,
                "time":2
             };
                   
              myDB.collection(colName).find({ "qid":qid }, { projection:  responseParams }).limit(reqBody.getTimeSlots.limit).toArray(function(err, result) {
                  if (err){
                      res.json({"status":"err"});
                  }else{
                      res.json({ "status":"time slots loaded success", result });
                      db.close();
                      return;
                  }
                });
          }


          //**GET ALL BOOKINGS
          if(name === 'get_bookings'){     
            myDB.collection(colName).find({ "qid":qid }).limit(reqBody.getBookings.limit).toArray(function(err, result) {
                if (err){
                    res.json({"status":"err"});
                }else{
                    res.json({ "status":"bookings loaded success", result });
                    db.close();
                    return;
                }
              });
          }

    
          //**MANAGE BOOKING (approved or canceled)
          if(name === 'manage_bookings'){
            //booking status should be pending and booking id should be matched

            //check _id
            if(ObjectID.isValid(reqBody.manageBookings.booking_id)){

              var myquery = { 
                "_id":ObjectId(reqBody.manageBookings.booking_id),
                "booking_status":"pending"
           };
            var newvalues = { 
              "booking_status":reqBody.manageBookings.booking_status
          };
          
            myDB.collection(colName).updateOne( myquery , {$set: newvalues }, function(err, result) {
              if (err){
                res.json({"status":"err"});
              }else{
                if(result !== null){
                  //check if updated or not
                  if(result.modifiedCount === 1){
                    //status updated success
                  res.json({"status":"status_updated_success"});

                       //**booking status is updated, now we send notification to user
                       //create user notification payload
                       //date and time
                       var currentdate = new Date(); 
                       var date =  currentdate.getFullYear() + "-"
                                       + (currentdate.getMonth()+1)  + "-" 
                                       + currentdate.getDate();     

                                       var time = currentdate.getHours() + ":"  
                                       + currentdate.getMinutes(); 
                       const userNotiPayload = {
                        "user_id": reqBody.manageBookings.notifyUser.user_id,
                        "message": "Hi, "+reqBody.manageBookings.notifyUser.user_name + " your booking is "
                        +reqBody.manageBookings.booking_status + " at " + reqBody.manageBookings.notifyUser.vendor_name,
                        "date":date,
                        "time":time
                       };
                      
                      myDB.collection("userNotificationsCol").insertOne(userNotiPayload, function(err, result) {
                          if (err){
                             //res.json({"status":"err"});
                          }else{
                            if(result !== null){
                              //res.json({"status":"notification sended to user success"}); 
                            }else{
                              //res.json({"status":"err"});
                            }

                            //close db connection
                         db.close();
                         return;
                         }
                 });

                  }else{
                    res.json({"status":"err"});
                  }

              }else{
                res.json({"status":"err"});
              }


              }
                    
            });
               
            }else{
              res.json({"status":"invalid booking_id"});
              return;
            }

             

          }


          //**CANCEL BOOKING (only after 15minutes of client appointment time)
          if(name === 'cancel_bookings'){
             //booking id should be matched

            //check _id
            if(ObjectID.isValid(reqBody.cancelBookings.booking_id)){

              //**get booking from db
              const responseParams =  { _id: 0,
                 "booking_details":1
              };
                   
              myDB.collection(colName).findOne({ "_id":ObjectId(reqBody.cancelBookings.booking_id) }, { projection:  responseParams } , function(err, result) {
                  if (err){
                      res.json({"status":"err"});
                  }else{
                        
                      if(result !== null){
                        //res.json({ "status":"booking loaded success", result });
                        //appointment datetime
                        var appoint_date = result.booking_details.appointment_date;
                        var appoint_time = result.booking_details.appointment_time;
                        var appoint_datetime = appoint_date + " " + appoint_time+":00Z";
                        var appointDateTime = new Date(appoint_datetime);
                       
                        //current datetime
                        var currentdate = new Date();
                        var date =  currentdate.getFullYear() + "-"
                                       + (currentdate.getMonth()+1)  + "-" 
                                       + currentdate.getDate();     
                                       var time = currentdate.getHours() + ":"  
                                       + currentdate.getMinutes();           
                        var mDateTime = new Date(date + " " + time+":00Z");
                        var mMinutes = mDateTime.setMinutes(mDateTime.getMinutes()-15); //(-15 minutes)
                        var currDateTime =  new Date(mMinutes);

                        //**check appointment datetime is greater than current time
                        if (appointDateTime < currDateTime) {
                          //we are eligible to cancelled

                          var myquery = { 
                            "_id":ObjectId(reqBody.cancelBookings.booking_id),
                       };
                        var newvalues = { 
                          "booking_status":reqBody.cancelBookings.booking_status
                      };
                      
                        myDB.collection(colName).updateOne( myquery , {$set: newvalues }, function(err, result) {
                          if (err){
                            res.json({"status":"err"});
                          }else{
                            if(result !== null){
                              //check if updated or not
                              if(result.modifiedCount === 1){
                                //status updated success
                              res.json({"status":"status_updated_success"});
            
                                   //**booking status is updated, now we send notification to user
                                   //create user notification payload
                                   //date and time
                                   var currentdate = new Date(); 
                                   var date =  currentdate.getFullYear() + "-"
                                                   + (currentdate.getMonth()+1)  + "-" 
                                                   + currentdate.getDate();     
            
                                                   var time = currentdate.getHours() + ":"  
                                                   + currentdate.getMinutes(); 
                                   const userNotiPayload = {
                                    "user_id": reqBody.cancelBookings.notifyUser.user_id,
                                    "message": "Hi, "+reqBody.cancelBookings.notifyUser.user_name + " your booking is "
                                    +reqBody.cancelBookings.booking_status + " at " + reqBody.cancelBookings.notifyUser.vendor_name,
                                    "date":date,
                                    "time":time
                                   };
                                  
                                  myDB.collection("userNotificationsCol").insertOne(userNotiPayload, function(err, result) {
                                      if (err){
                                         //res.json({"status":"err"});
                                      }else{
                                        if(result !== null){
                                          //res.json({"status":"notification sended to user success"}); 
                                        }else{
                                          //res.json({"status":"err"});
                                        }
            
                                        //close db connection
                                     db.close();
                                     return;
                                     }
                             });
            
                              }else{
                                res.json({"status":"err"});
                              }
            
                          }else{
                            res.json({"status":"err"});
                          }
            
            
                          }
                                
                        });

                        }else{
                          res.json({"status":"err not eligible to cancel"});
                          return;
                        }
                
                        

                      }else{
                        res.json({"status":"err"});
                      }
                      
                  }
                });

 
              
               
            }else{
              res.json({"status":"invalid booking_id"});
              return;
            }
          }

          //**GET ALL REVIEWS
          if(name === 'get_reviews'){
            myDB.collection(colName).find({ "vendor_id":qid }).limit(reqBody.getReviews.limit).toArray(function(err, result) {
              if (err){
                  res.json({"status":"err"});
              }else{
                  res.json({ "status":"reviews loaded success", result });
                  db.close();
                  return;
              }
            });
          }
           
        }
        
      });


    }

}());




                   


