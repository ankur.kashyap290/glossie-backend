(function() {
    //var someThings = ...;
    module.exports.executeApi = function(user_id, colName, res, name, reqBody, mongo, mongodbBaseUrl, MONGO_DATABASE_NAME) {
       // res.send( 'Hello Nodejs: '+name+' params: '+JSON.stringify(reqBody) );
       // return someThings();

       //connect to mongoDB
       const options = {useUnifiedTopology: true};
       mongo.connect(mongodbBaseUrl ,options, function(err, db) {
        if (err){
           //not connected to mongodb
           throw err;
        }else{
          //we are successfully connected to mongodb
          //console.log("Connected to mongodb");
          //specify our db name
          var myDB = db.db(MONGO_DATABASE_NAME);
          
          //**REGISTRATION
          if(name === 'register'){
          //check if user already exists or not
          //check number
          myDB.collection(colName).findOne({"number":reqBody.number}, function(err, result) {
            if (err){
                //an err occured
                res.json({"status":"err"});
                return;
            }else{
                if(result !== null){
                    //number already exists
                    res.json({"status":"number_already_exists"});
                    return;
                }else{
                   //number not exists, now we check email
                   myDB.collection(colName).findOne({"email":reqBody.email}, function(err, result) {
                    if (err){
                        //an err occured
                        res.json({"status":"err"});
                        return;
                    }else{
                        if(result !== null){
                            //email already exists
                            res.json({"status":"email_already_exists"});
                            return;
                        }else{
                           //number and email not exists
                            //**user not exists, now procced to create new user account
                           myDB.collection(colName).insertOne(reqBody, function(err, result) {
                             if (err){
                                res.json({"status":"err"});
                             }else{
                               if(result !== null){
                                res.json({"status":"User registered successfully"});
                               }else{
                                res.json({"status":"err"});
                               }
                            }
  
                            //close db connection
                            db.close();
                            return;
                        
                      });
        
                        }
                    }
                  });
                    

                }
            }
          });

          }

          //**LOGIN
          if(name === 'login'){
            myDB.collection(colName).findOne({ "email":reqBody.email, "pass":reqBody.pass }, function(err, result) {
                if (err){
                    res.json({"status":"err"});
                    return;
                }else{
                    //check user login
                    if(result !== null){
                    //validate login
                    if(result.email === reqBody.email 
                        && result.pass === reqBody.pass){
                            const userLoggedInData = { "status":"login success" , "email":result.email, "name":result.name, "number":result.number };
                            //**login success
                            res.json(userLoggedInData);
                        }else{
                            //invalid credentials
                            res.json( {"status":"login failed invalid credentials"} ); 
                        }    
                    }else{
                     //invalid credentials
                     res.json( {"status":"login failed invalid credentials"} ); 
                    }

                    db.close();
                    return;
                }
                
              });
          }

          //**GET ALL VENDORS (active)
          if(name === 'get_vendors'){

            const responseParams =  { _id: 0,
                "number": 1,
                "email": 2,
                "name": 3,
                "description": 4,
                "salon_name": 5,
                "salon_for": 6,
                "website_name": 7,
                "salon_img": 8,
                "salon_logo": 9,
                "salon_address": 10,
                "salon_zipcode": 11,
                "salon_city": 12,
                "salon_state": 13,
                "salon_country": 14,
                "salon_timing": 15,
                "created_at": 16,
                "updated_at": 17,
                "_id": 18
        };

            myDB.collection(colName).find({"status":"active",  "salon_zipcode":reqBody.getVendors.search_by_zipcode}, { projection:  responseParams } ).limit(reqBody.getVendors.limit).toArray(function(err, result) {
                if (err){
                    res.json({"status":"err"});
                }else{
                    res.json({ "status":"vendors loaded success", result });
                    db.close();
                    return;
                }
              });
          }

          //**GET ALL TIME SLOTS OF VENDOR
          if(name === 'get_vendor_time_slots'){
            const responseParams =  { _id: 0,
              "time": 1,
              "_id": 2
           };

          myDB.collection(colName).find({"qid":reqBody.getVendorTimeSlots.qid}, { projection:  responseParams } ).toArray(function(err, result) {
              if (err){
                  res.json({"status":"err"});
              }else{
                  res.json({ "status":"vendor time slots loaded success", result });
                  db.close();
                  return;
              }
            });
          }

          //**GET ALL SERVICES OF VENDOR
          if(name === 'get_vendor_services'){
            const responseParams =  { _id: 0,
              "_id": 1,
              "name": 2,
              "category": 3,
              "image": 4,
              "service_for": 5,
              "service_price": 6,
              "service_minutes": 7
           };
            

          myDB.collection(colName).find({"status":"visible", "qid":reqBody.getVendorServices.qid}, { projection:  responseParams } ).toArray(function(err, result) {
              if (err){
                  res.json({"status":"err"});
              }else{
                  res.json({ "status":"vendor services loaded success", result });
                  db.close();
                  return;
              }
            });
          }


          //**ADD VENDOR BOOKING (OFFLINE)
          if(name === 'add_vendor_booking_offline'){

            //check appointment_date & appointment_time
            myDB.collection(colName).findOne({"appointment_date":reqBody.addVendorBookingOffline.booking_details.appointment_date
           ,"appointment_time":reqBody.addVendorBookingOffline.booking_details.appointment_time}, function(err, result) {
              if (err){
                  //an err occured
                  res.json({"status":"err"});
                  return;
              }else{
                  if(result !== null){
                      //booking already exists on this time
                      res.json({"status":"booking_already_exists_on_this_time"});
                      return;
                  }else{

                     //**first we need delete time slot
                     myDB.collection("vendorTimeSlotCol").deleteOne({"date":reqBody.addVendorBookingOffline.booking_details.appointment_date,
                      "time":reqBody.addVendorBookingOffline.booking_details.appointment_time,
                      "qid":reqBody.addVendorBookingOffline.qid }, function(err, result) {
                      if (err){
                        res.json({"status":"err"});
                        return;
                      }else{
                        //check if removed success
                        if(result.deletedCount === 1){
                            //time slot removed success
                            //now we procced to add booking
                            //**date and time of appointment not exists now we add new booking
                            var currentdate = new Date(); 
                            var date =  currentdate.getFullYear() + "-"
                                            + (currentdate.getMonth()+1)  + "-" 
                                            + currentdate.getDate();     

                                            var time = currentdate.getHours() + ":"  
                                            + currentdate.getMinutes();                 
                                            
                           //add additional data in existing reqBody                 
                           reqBody.addVendorBookingOffline.booking_details.booking_date = date;//(booking_date)
                           reqBody.addVendorBookingOffline.booking_details.booking_time = time;//(booking_time)
                           reqBody.addVendorBookingOffline.booking_status = "pending";//(booking_status)
                           reqBody.addVendorBookingOffline.paid = "UNPAID";//(paid)
                         
                           myDB.collection(colName).insertOne(reqBody.addVendorBookingOffline, function(err, result) {
                             if (err){
                                res.json({"status":"err"});
                             }else{
                               if(result !== null){
                                 res.json({"status":"Booking created successfully"}); 

                               }else{
                                 res.json({"status":"err"});
                               }
                            }

                            //close db connection
                            db.close();
                            return;
                        
                      });

                        }else{
                            //an error occured
                            res.json({"status":"err invalid appointment time"})
                         }
                      }
                    });

                  }
              }
            });

          }

          
          //**GET USER NOTIFICATIONS
          if(name === 'get_user_notifications'){
            const responseParams =  { _id: 0,
              "_id": 1,
              "message": 2,
              "date": 3,
              "time": 4
           };
            
          myDB.collection(colName).find({"user_id":user_id}, { projection:  responseParams } ).toArray(function(err, result) {
              if (err){
                  res.json({"status":"err"});
              }else{
                  res.json({ "status":"user notifications loaded success", result });
                  db.close();
                  return;
              }
            });
          }

          //**GET ALL HOME SERVICES OF ADMIN 
          if(name === 'get_admin_services'){
            const responseParams =  { _id: 0,
              "_id": 1,
              "name": 2,
              "category": 3,
              "image": 4,
              "service_for": 5,
              "service_price": 6,
              "service_minutes": 7
           };
            

          myDB.collection(colName).find({"status":"visible", "service_for":reqBody.getAdminServices.service_for}, { projection:  responseParams } ).toArray(function(err, result) {
              if (err){
                  res.json({"status":"err"});
              }else{
                  res.json({ "status":"admin services loaded success", result });
                  db.close();
                  return;
              }
            });
          }

          //**ADD ADMIN BOOKING (OFFLINE)
          if(name === 'add_admin_booking_offline'){
            //add booking
                            var currentdate = new Date(); 
                            var date =  currentdate.getFullYear() + "-"
                                            + (currentdate.getMonth()+1)  + "-" 
                                            + currentdate.getDate();     

                                            var time = currentdate.getHours() + ":"  
                                            + currentdate.getMinutes();                 
                                            
                           //add additional data in existing reqBody                 
                           reqBody.addAdminBookingOffline.booking_details.booking_date = date;//(booking_date)
                           reqBody.addAdminBookingOffline.booking_details.booking_time = time;//(booking_time)
                           reqBody.addAdminBookingOffline.booking_status = "pending";//(booking_status)
                           reqBody.addAdminBookingOffline.paid = "UNPAID";//(paid)
                           reqBody.addAdminBookingOffline.employee_name = "none"; //(employee_name)
                         
                           myDB.collection(colName).insertOne(reqBody.addAdminBookingOffline, function(err, result) {
                             if (err){
                                res.json({"status":"err"});
                             }else{
                               if(result !== null){
                                 res.json({"status":"Booking created successfully"}); 

                               }else{
                                 res.json({"status":"err"});
                               }
                            }

                            //close db connection
                            db.close();
                            return;
                        
                      });
          }

          //**REVIEW FOR EMPLOYEE
          if(name === 'add_employee_review'){

            //date and time
            var currentdate = new Date(); 
            var date =  currentdate.getFullYear() + "-"
                            + (currentdate.getMonth()+1)  + "-" 
                            + currentdate.getDate();     

                            var time = currentdate.getHours() + ":"  
                            + currentdate.getMinutes(); 
            var employeeReviewPayload = {
              "employee_id": reqBody.addEmployeeReview.employee_id,
              "review": reqBody.addEmployeeReview.review,
              "rate": reqBody.addEmployeeReview.rate,
              "user_name": reqBody.addEmployeeReview.user_name,

              "date":date,
              "time":time
             };

         myDB.collection(colName).insertOne(employeeReviewPayload, function(err, result) {
        if (err){
           //res.json({"status":"err"});
        }else{
          if(result !== null){
            res.json({"status":"employee review submit success"}); 
          }else{
            res.json({"status":"err"});
          }

        //close db connection
       db.close();
       return;

       
       }
        });

          }

          //**REVIEW FOR VENDOR
          if(name === 'add_vendor_review'){
              //date and time
              var currentdate = new Date(); 
              var date =  currentdate.getFullYear() + "-"
                              + (currentdate.getMonth()+1)  + "-" 
                              + currentdate.getDate();     
  
                              var time = currentdate.getHours() + ":"  
                              + currentdate.getMinutes(); 
              var vendorReviewPayload = {
                "vendor_id": reqBody.addVendorReview.vendor_id,
                "review": reqBody.addVendorReview.review,
                "rate": reqBody.addVendorReview.rate,
                "user_name": reqBody.addVendorReview.user_name,
                "date":date,
                "time":time
               };
  
           myDB.collection(colName).insertOne(vendorReviewPayload, function(err, result) {
          if (err){
             //res.json({"status":"err"});
          }else{
            if(result !== null){
              res.json({"status":"vendor review submit success"}); 
            }else{
              res.json({"status":"err"});
            }
  
          //close db connection
         db.close();
         return;
  
         
         }
          });
          }

           
        }
        
      });


    }

}());


                   


