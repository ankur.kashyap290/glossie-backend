
(function() {
    module.exports.executeApi = function(employee_id, colName, res, name, reqBody, mongo, mongodbBaseUrl, MONGO_DATABASE_NAME) {

       //connect to mongoDB
       const options = {useUnifiedTopology: true};
       mongo.connect(mongodbBaseUrl ,options, function(err, db) {
        if (err){
           //not connected to mongodb
           throw err;
        }else{
          //we are successfully connected to mongodb
          //console.log("Connected to mongodb");
          //specify our db name
          var myDB = db.db(MONGO_DATABASE_NAME);

          //**LOGIN
          if(name === 'login'){
            myDB.collection(colName).findOne({ "email":reqBody.email, "pass":reqBody.pass }, function(err, result) {
                if (err){
                    res.json({"status":"err"});
                    return;
                }else{
                    //check user login
                    if(result !== null){
                    //validate login
                    if(result.email === reqBody.email 
                        && result.pass === reqBody.pass){
                            //remove some keys
                            delete result._id;
                            delete result.pass;
                            delete result.created_at;
                            delete result.updated_at;
                            //**login success
                            res.json({"status":"login success", result});
                        }else{
                            //invalid credentials
                            res.json( {"status":"login failed invalid credentials"} ); 
                        }    
                    }else{
                     //invalid credentials
                     res.json( {"status":"login failed invalid credentials"} ); 
                    }

                    db.close();
                    return;
                }
                
              });
          }


          //** GET ALL EMPLOYEE NOTIFICATIONS
          if(name === 'get_employee_notifications'){
            const responseParams =  { _id: 0,
                "_id": 1,
                "message": 2,
                "date": 3,
                "time": 4,
                "user_address":5
             };
              
            myDB.collection(colName).find({"employee_id":employee_id}, { projection:  responseParams } ).toArray(function(err, result) {
                if (err){
                    res.json({"status":"err"});
                }else{
                    res.json({ "status":"employee notifications loaded success", result });
                    db.close();
                    return;
                }
              });

          }

          //** GET ALL REVIEWS
          if(name === 'get_reviews'){
            myDB.collection(colName).find({ "employee_id":employee_id }).limit(reqBody.getReviews.limit).toArray(function(err, result) {
                if (err){
                    res.json({"status":"err"});
                }else{
                    res.json({ "status":"reviews loaded success", result });
                    db.close();
                    return;
                }
              });
          }
          

        }

        });
        }
  }());         