const configs = function() {
	switch (process.env.NODE_ENV) {
		// case 'development':
		//   return {
		//     APP_URL: 'https://demo.local',
		//     API_URL: 'http://localhost:8282/api',
		//     API_IMAGE_URL: 'http://localhost:8282',
		//     MONGO_HOST: 'localhost',
		//     MONGO_PORT: '27017',
		//     MONGO_DB_NAME: 'glossie',
		//   };

		// case 'production':
		//   return {
		//     APP_URL: 'https://glossie.com',
		//     API_URL: 'https://glossieapi.com/api',
		//     API_IMAGE_URL: 'http://localhost:8282',
		//     MONGO_HOST: 'localhost',
		//     MONGO_PORT: '28000',
		//     MONGO_DB_NAME: 'glossie',
		//   };

		default:
			return {
				APP_URL: 'http://localhost:3000',
				API_URL: 'http://localhost:8282/api',
				API_IMAGE_URL: 'http://localhost:8282',
				MONGO_HOST: 'localhost',
				MONGO_PORT: '27017',
				MONGO_DB_NAME: 'glossie',
			};
	}
};

const settings = {
	...configs(),
};

module.exports = settings;
